const tokenSets = require('../api/tokenSets');
var BigNumber = require('big-number');

const tokenSetsPricing = async ({ isString, id, convertFrom, convertTo, amount }) => {
    let tokenSetsPrice = (await tokenSets.fetchTokensSetsAPI({ id }));  // fetching the API
    tokenSetsPrice = parseFloat(tokenSetsPrice) // converting it to floating number
    console.log("The price of 1(10**8) uints of tokenSets is " + tokenSetsPrice)
    const ethereumPrice = Number(await tokenSets.fetchEtherPriceAPI({ convertTo, convertFrom }));
    const totalUSDAmount = (ethereumPrice * amount);
    console.log("the price of 1(10**18) unit of ETH is " + totalUSDAmount)
    console.log("the number of units you will get for 1 ETH is " + (totalUSDAmount / tokenSetsPrice))
    console.log("the number of units you will get for 0.1 ETH is " + (totalUSDAmount / tokenSetsPrice) * 0.1)
    let x = (((((totalUSDAmount / tokenSetsPrice) / BigNumber(10 ** 18)) * BigNumber(100000000000000000)) * BigNumber(10 ** 18)) - BigNumber(50000000000000000))


    if (isString) {
        return String(x);
    }
    return Number(x);
};

module.exports = { tokenSetsPricing };