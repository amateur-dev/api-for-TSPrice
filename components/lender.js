/**
 * This file has a bunch commented out code that was earlier used
 * to calculate the amount of ETHHIVOL pricing.
 * In case we get ETHHIVOL working, we can display that stuff using this.
 */
const coinGecko = require('../api/coinGecko');

// const tokenSets = require('../api/tokenSets');

const cdaiAndEthhivolPricing = async ({ amount }) => {
    const cdaiPercent = 0.85;     // 85% cDai
    // const ethhivolPercent = 0.15; // 15% ETHHIVOL
    const amountOfCdaiInEth = cdaiPercent * amount;
    // const amountOfEthihivolInEth = ethhivolPercent * amount;
    const cdaiToEth = await coinGecko.currencyConversion({
        convertFrom: 'cdai', convertTo: 'eth' });
    // const ethhivolToUsd = await tokenSets.fetchTokensSetsAPI({ id: 'ethhivol' });
    // const ethToUsd = await coinGecko.currencyConversion({
        // convertFrom: 'ethereum', convertTo: 'usd' });
    // const usdToEth = 1/ethToUsd;
    // const ethihivolToUsd = ethhivolToUsd * usdToEth;
    const totalCdaiHeld = amountOfCdaiInEth * cdaiToEth;
    // const totalEthihivolHeld = amountOfEthihivolInEth * ethihivolToUsd;

    return {
        cdai: totalCdaiHeld,
        // ethhivol: totalEthihivolHeld
    };
}

module.exports = { cdaiAndEthhivolPricing };