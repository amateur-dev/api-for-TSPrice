const tokenSets = require('../api/tokenSets');

const pricing = async ({ amount, id, convertFrom, convertTo }) => {
    const tokenSetsPrice = Number(await tokenSets.fetchTokensSetsAPI({ id }));  // Id is the input Id for the rebalancing set we want.
    const ethereumPrice = Number(await tokenSets.fetchEtherPriceAPI({ convertTo, convertFrom}));
    const totalUSDAmount = ethereumPrice * amount;
    const amountInvestedInTokenSets = totalUSDAmount/4;
    // Keeping the .98 conversion that existed initially.
    let output = amountInvestedInTokenSets/tokenSetsPrice * 0.98;
    output = output*(10**18); // Converting it to a string
    
    // Feel free to comment out for debugging purposes.    
    // console.log('tokenSetsPrice ', tokenSetsPrice);
    // console.log('ethereumPrice ', ethereumPrice);
    // console.log('totalUSDAmount ', totalUSDAmount);
    // console.log('amountInvestedInTokenSets ', amountInvestedInTokenSets);
    // console.log('output ', output);
    
    return output;
}

module.exports = { pricing };