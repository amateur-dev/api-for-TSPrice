const apiHelpers = require('./apiHelpers');

const COINGECKO_URL = "https://api.coingecko.com/api";

const VS_CURRENCY_URL = `${COINGECKO_URL}/v3/simple/price?ids=:convertFrom&vs_currencies=:convertTo`;

const currencyConversion = async ({ convertTo, convertFrom }) => {
  const url = `${VS_CURRENCY_URL}`.replace(':convertFrom', convertFrom)
      .replace(':convertTo', convertTo);
  const response = await apiHelpers.fetchRequest(
    url, () => apiHelpers.buildOptions());
  return response[convertFrom][convertTo];
};

module.exports = { currencyConversion };
