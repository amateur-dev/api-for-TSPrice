const requestPromise = require('request-promise');

const buildHeaders = (authorizationValue, accept = 'application/json') => {
  if (!authorizationValue) {
    return ({
      Accept: accept,
      'content-type': 'application/json',
    });
  }
  return ({
    Accept: accept,
    Authorization: authorizationValue,
  });
};

const buildOptions = (endpoint, method = 'GET', authorizationValue, body) => {
  const headers = buildHeaders(authorizationValue);
  const options = {
    method,
    uri: endpoint,
    headers,
    json: true,
    body,
  };

  return options;
};

const fetchRequest = (endpoint, authorizationValue, method, body) => requestPromise(buildOptions(endpoint, method, authorizationValue, body))
  .catch((error) => {
    // eslint-disable-next-line no-console
    console.log(`Error while making a ${method} call on ${endpoint}`, `With Request ${body}`, error);
    throw error;
  });

module.exports = {
  fetchRequest,
};
