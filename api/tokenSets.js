const apiHelpers = require('./apiHelpers');

const tokenSetsURL = "https://api.tokensets.com/v1/rebalancing_sets/";
const tokenSetsPublicURL = 'https://api.tokensets.com/public/v1/rebalancing_sets';
const etherscanURL = "https://api.coingecko.com/api/v3/simple/price?ids=:convertFrom&vs_currencies=:convertTo";

const fetchTokensSetsAPI = async ({ id }) => {
    const response = await apiHelpers.fetchRequest(
      `${tokenSetsURL}/${id}`,
      () => apiHelpers.buildOptions()
    );
    return response.rebalancing_set.price_usd;
};

const fetchEtherPriceAPI = async ({ convertTo, convertFrom }) => {
  const url = `${etherscanURL}`.replace(':convertFrom', convertFrom)
      .replace(':convertTo', convertTo);
  const response = await apiHelpers.fetchRequest(
    url, () => apiHelpers.buildOptions());
  return response[convertFrom][convertTo];
};

const fetchRebalancingSet = async ()  => {
  const response = await apiHelpers.fetchRequest(`${tokenSetsPublicURL}`, () => apiHelpers.buildOptions());
  return response;
}

module.exports = { fetchTokensSetsAPI, fetchEtherPriceAPI, fetchRebalancingSet };
