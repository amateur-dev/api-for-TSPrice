const express = require('express');
const isEmpty = require('lodash/isEmpty');

const test = require('../components/test');

const router = express.Router();
/**
 * This works by passing a parameter of isString as true or false
 * If you don't pass any parameter, we consider it to be false by default
 * 
 * The Standard response is
 *   {
 *     "price": 'price'
 *   }
 */


router.get('/', function (req, res, next) {
    const isString = isEmpty(req.query.isString) ?
        false : req.query.isString === 'true' ? true : false;
    const id = isEmpty(req.query.id) ? 'ethhivol' : req.query.id;
    const amount = isEmpty(req.query.amountInEth) ? 1 : req.query.amountInEth;
    const convertFrom = isEmpty(req.query.convertFrom) ? 'ethereum' : req.query.convertFrom;
    const convertTo = isEmpty(req.query.convertTo) ? 'usd' : req.query.convertTo;
    return test.tokenSetsPricing({ isString, id, convertFrom, convertTo, amount })
        .then(qty => {
            res.status(200).send({
                qty,
            });
        });
});

module.exports = router;
