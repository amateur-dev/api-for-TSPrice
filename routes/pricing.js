const express = require('express');
const isEmpty = require('lodash/isEmpty');

const pricing = require('../components/pricing');

const router = express.Router();

router.get('/', async (req, res, next) => {
    const id = isEmpty(req.query.id) ? 'eth20smaco' : req.query.id;
    const amount = isEmpty(req.query.amountInEth) ? 1: req.query.amountInEth;
    const convertFrom = isEmpty(req.query.convertFrom) ? 'ethereum': req.query.convertFrom;
    const convertTo = isEmpty(req.query.convertTo) ? 'usd': req.query.convertTo;
    const quantity = await pricing.pricing({ amount, id, convertFrom, convertTo });
    res.status(200).send({
        qty: quantity,
    });
});

module.exports = router;
