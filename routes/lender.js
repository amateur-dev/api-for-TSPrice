const express = require('express');
const isEmpty = require('lodash/isEmpty');

const lender = require('../components/lender');

const router = express.Router();

router.get('/', async (req, res, next) => {
    const amount = isEmpty(req.query.amount) ? 1: req.query.amount;
    const { cdai, ethhivol } = await lender.cdaiAndEthhivolPricing({ amount });
    res.status(200).send({
        cdai, ethhivol,
    });
});

module.exports = router;
