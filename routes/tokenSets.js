const express = require('express');

const tokenSets = require('../api/tokenSets');

const router = express.Router();

router.get('/', async (req, res) => {
    const { rebalancing_sets } = await tokenSets.fetchRebalancingSet();
    res.status(200).send({
        rebalancing_sets,
    });
});

module.exports = router;
